package in.mondays.nfc.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import in.mondays.nfc.Fragments.AccessFragment;
import in.mondays.nfc.Fragments.StudentFragment;


/**
 * * Created by hp1 on 21-01-2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are
    // Going to be passed when ViewPagerAdapter is
    // created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when
    // the ViewPagerAdapter is created
    AccessFragment tab1;
    StudentFragment tab2;


    // Build a Constructor and assign the passed Values to appropriate values in
    // the class
    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[],
                            int mNumbOfTabsumb) {
        super(fm);
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;



    }

    // This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if (position == 0) // if the position is 0 we are returning the First
        // tab
        {
            tab1 = new AccessFragment();

            return tab1;
        } else if (position == 1) // if the position is 1 we are returning the Second tab
        {
           /* VisitorList tab2 = new VisitorList();*/

            tab2 = new StudentFragment();
            // tab2=new VisitorFragment();
            return tab2;
        }


        return null;


    }


    public Fragment getFragment(int position)
    {
        if (position == 0) // if the position is 0 we are returning the First
        // tab
        {

            return tab1;
        } else if (position == 1) // if the position is 1 we are returning the Second tab
        {
           /* VisitorList tab2 = new VisitorList();*/


            return tab2;
        }

        return null;

    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        // TODO Auto-generated method stub


        return super.getItemPosition(object);
    }
}