package in.mondays.nfc.Util;


public class Global {


    public static String PROJECT_ID = "293044879387";  // "558700389898"
    public static String SERVER_ERROR_MSG = "Server error! please try after some time";
    public static String NO_INTERNET_MSG = "No Internet Connection! Try again";
    public static String CHECK_INTERNET_MSG = "Check your internet connection and try again!";



    public static final String BYTESPARA1 = "bytespara1";


    public static final String INTPARA1 = "intpara1";
    public static final String INTPARA2 = "intpara2";
    public static final int CMD_POS_WRITE = 100100;
    public static String APP_NAME="nfcWriter";
    public static String BIZ_ID ="biz_id";
    public static String BIZ_PERSON_NAME ="biz_person_name"; //ChairmanName
    public static  String BIZ_COMPANY_NAME = "biz_company_name";
    public static String BIZ_COMPANY_IMG = "ProfileImg";
    public static String BIZ_EXPIRY_DATE = "expiryDate";
    public static  String LOGIN_SP ="Login_NFC";
    public static String VIZ_IMAGE_SP="VIZ_IMAGE";

    public static String SETTINGS_PHOTO_REQUIRED = "photoRequired";
    public static String SETTINGS_PRINT_REQUIRED = "printRequired";
    public static String SETTINGS_PRINT_PHOTO ="printImageRequired";
    public static String SETTINGS_CONFIRM_REQUIRED ="confirmRequired";
    public static String SETTINGS_CHECKOUT_TIME ="checkOutTime";
    public static String SETTINGS_SLIP_FOOTER_MSG="Authentication_Array";
    public static String SETTINGS_SLIP_FOOTER_MSG_SIZE="Authentication_Array_size";
    public static String SETTINGS_SLIP_BRANDING_MSG="BrandingMessageArray";
    public static String SETTINGS_SLIP_BRANDING_MSG_SIZE="BrandingMessageArray_size";
    public static String SETTINGS_SLIP_FOOTER_LINE="Authentication_Array_";
    public static String SETTINGS_SLIP_HEADER_LINE="HeaderMessageLine_";
    public static String IS_LOGGED_IN="IsLoggedIn";
    public static String SETTINGS_SLIP_BRANDING_LINE="BrandingMessageLine_";

    public static String SETTINGS_SLIP_HEADER_MSG="HeaderMessageArray";
    public static String SETTINGS_SLIP_HEADER_MSG_SIZE="HeaderMessageArray_size";

    public static String BLUETOOTH_SP = "Bluetooth";
    public static String DEVICE_NAME ="device_name";
    public static String SETTINGS_AUTO_CUT_MOBILE = "autoCutMobile";
    public static String VERSION_NUMBER= "verisonNumber";
    public static String GROUP_ID="groupId";
    public static final String UUID="uuid";
    public static final String GROUP_NAME="groupName";
    public static final String FIREBASE_TOKEN="firebaseToken";


    public static final int CALL_DIALED=1;
    public static final int CALL_MANNUAL=2;
    public static final int CALL_VENDOR_PASS_SCAN=3;
    public static final int IN_VENDOR_PASS_NFC=4;

    public static final int OUT_SCAN_QR=1;
    public static final int OUT_MANNUAL=2;
    public static final int OUT_VENDOR_PASS_SCAN=3;
    public static final int OUT_VENDOR_PASS_NFC=4;


    public static final String SELECTED_TYPE="selectedType";
    //Access codes

    public static final String STAFF="ST";
    public static final String RESIDENT="RE";
    public static final String PARENT="PA";
    public static final String VISITOR ="VI";
    public static final String VENDOR="VE";
    public static final String STUDENT="STU";


    //Custom Fields

    public static final String TEXTBOX="tb";
    public static final String DROPDOWN="dd";
    public static final String IMG="img";


    //node names

/*    public static final String DEVICE_REF="device/";
    public static final String ACCESS_REF="access/";
    public static final String ACCESS_HISTORY_REF="accessHistory/";
    public static final String ADMIN_REF="admin/";
    public static final String APPOINTMENT_REF="appt/";
    public static final String BIZ_REF="biz/";
    public static final String DEVICE_SETTING_REF="deviceSetting/";
    public static final String FIELD_REF="field/";
    public static final String MOBBIZ_REF="mobBiz/";
    public static final String PURPOSE_REF="purpose/";
    public static final String VISIOR_REF="visitor/";
    public static final String VISITOR_PROFILE="visitorProfile/";
    public static final String WAIT_REF="wait/";*/
}
