package in.mondays.nfc.Fragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import in.mondays.nfc.CustomControls.SimpleDividerItemDecoration;
import in.mondays.nfc.MainActivity;
import in.mondays.nfc.PersistentDatabase;
import in.mondays.nfc.R;
import in.mondays.nfc.Util.CommonMethods;
import in.mondays.nfc.Util.Global;
import in.mondays.nfc.adapter.VisitorListAdapter;
import in.mondays.nfc.gettersetter.Visitor;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccessFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AccessFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccessFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    ArrayList<Visitor> accessList;
    RecyclerView rvVisitors;
    VisitorListAdapter adapter;
    DatabaseReference visitorRef=null;


    EditText editSearch;
    Button btnScan;
    EditText ed_barcode;


    ChildEventListener vizChildListner=null;
    SharedPreferences loginsp;


    DatabaseReference lblRef;
    HashMap<String, String> hashMapLbl;
    String type;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    public AccessFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccessFragment.
     */

    // TODO: Rename and change types and number of parameters
    public static AccessFragment newInstance(String param1, String param2) {
        AccessFragment fragment = new AccessFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =   inflater.inflate(R.layout.fragment_access, container, false);

        initGUI(view);

        return view;
    }

    private void initGUI(View view)
    {
        type = getArguments().getString("type");
        rvVisitors = (RecyclerView)view.findViewById(R.id.listview);
        loginsp=getActivity().getSharedPreferences(Global.LOGIN_SP,Context.MODE_PRIVATE);

        accessList =new ArrayList<>();
        hashMapLbl=new HashMap<>();
        hashMapLbl.put("All","All");


        adapter = new VisitorListAdapter(getActivity(),accessList, CommonMethods.getAccessType(type));
        // Attach the toMeetadapter to the recyclerview to populate items
        rvVisitors.setAdapter(adapter);
        rvVisitors.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        rvVisitors.setLayoutManager(new LinearLayoutManager(getActivity()));

        visitorRef = PersistentDatabase.getDatabase().getReference(type.toLowerCase()+"/"
                +loginsp.getString(Global.BIZ_ID,"")+"/"+loginsp.getString(Global.GROUP_ID,""));
        visitorRef.keepSynced(true);
        lblRef= PersistentDatabase.getDatabase().getReference("label/"+
                loginsp.getString(Global.BIZ_ID,"")+"/"+loginsp.getString(Global.GROUP_ID,"")+"/"+type.toLowerCase());
        lblRef.keepSynced(true);
        getVisitorData(getActivity(),"All");
        lblRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists())
                {
                    hashMapLbl.put(dataSnapshot.getKey(),dataSnapshot.child("nm").getValue(String.class));
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
       /* if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String lbl,String type );
    }

    public void getVisitorData(final Context context, final String lblKey)
    {


        if(vizChildListner!=null)
        {
            visitorRef.removeEventListener(vizChildListner);
            accessList.clear();
            /*adapter.notifyDataSetChanged();*/
            adapter.customDatasetChanged();

        }


        // Query query1=visitorRef.orderByChild("in").startAt(getYesterdaysTime()).endAt(getTodaysTime());


        final int count=0;

        try {
            vizChildListner=visitorRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(final DataSnapshot child, String s) {
                    try {
                        if (lblKey.equals("All") || child.child("lbl").hasChild(lblKey)) {
                            Visitor visitor = new Visitor();
                            visitor.setName(child.child("nm").getValue().toString());
                            visitor.setMobile(child.child("mob").getValue().toString());
                            if (child.hasChild("img"))
                                visitor.setPhotoUrl(child.child("img").getValue().toString());
                            if (child.hasChild("nfc"))
                                visitor.setIssued(child.child("nfc").getValue(Boolean.class));
                            visitor.setVisitorId(child.getKey());
                            accessList.add(0, visitor);
                            //adapter.notifyDataSetChanged();
                            adapter.customDatasetChanged();
                        }
                    }
                    catch(Exception e)
                    {
                        FirebaseCrash.report(e);
                    }



                }






                @Override
                public void onChildChanged(DataSnapshot child, String s) {

                    try {
                        if (lblKey.equals("All") || child.child("lbl").hasChild(lblKey)) {
                            Visitor visitor = new Visitor();
                            if (child.hasChild("img"))
                                visitor.setPhotoUrl(child.child("img").getValue().toString());
                            visitor.setName(child.child("nm").getValue().toString());
                            visitor.setMobile(child.child("mob").getValue().toString());
                            visitor.setVisitorId(child.getKey());
                            if (child.hasChild("nfc"))
                                visitor.setIssued(child.child("nfc").getValue(Boolean.class));
                            Iterator<Visitor> it = accessList.iterator();
                            int count = 0;
                            while (it.hasNext()) {
                                Visitor visitor1 = it.next();
                                if (visitor1.getVisitorId().equals(child.getKey())) {
                                    accessList.set(count, visitor);
                                    //adapter.notifyDataSetChanged();
                                    // toMeetadapter.notifyItemChanged(count);
                                    adapter.customDatasetChanged();

                                }
                                count++;
                            }


                        }

                            /*showSearchAndFilter();*/
                    }

                    catch (Exception e)
                    {

                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot child) {


                    Iterator<Visitor> it = accessList.iterator();
                    int count=0;
                    while (it.hasNext()) {
                        Visitor visitor1 = it.next();
                        if (visitor1.getVisitorId().equals(child.getKey())) {
                            it.remove();

                            //adapter.notifyDataSetChanged();
                            adapter.customDatasetChanged();
                        }
                        count++;
                    }



                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        catch (Exception e)
        {
            // Toast.makeText(context, Global.SERVER_ERROR_MSG, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);


        MenuItem searchItem = menu.findItem(R.id.action_in_search);
        SearchManager searchManager = (SearchManager)getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setMaxWidth(600);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(true);

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextChange(String newText)
            {
/*
               if ( TextUtils.isEmpty ( newText ) ) {

//                    if(accessList.size()>0)
//                        adapter.getFilter().filter("");

                } else{
                    adapter.getFilter().filter(newText);
                }*/

                //System.out.println("Called here");

                String search = newText.toString().trim();

/*                if(search.length() > 0) {

                    adapter.getFilter().filter(newText);
                }*/

                adapter.getFilter().filter(search);

                return true;

            }
            @Override
            public boolean onQueryTextSubmit(String query)
            {


                String search = query.toString().trim();

                /*if(search.length() > 0) {

                    adapter.getFilter().filter(query);
                }*/

                adapter.getFilter().filter(search);

                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // inflater.inflate(R.menu.menu_item, menu);
/*
        MenuItem searchItem = menu.findItem(R.id.action_in_search);
        SearchManager searchManager = (SearchManager)getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setMaxWidth(600);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(true);

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextChange(String newText)
            {
*//*
               if ( TextUtils.isEmpty ( newText ) ) {

//                    if(accessList.size()>0)
//                        adapter.getFilter().filter("");

                } else{
                    adapter.getFilter().filter(newText);
                }*//*

                //System.out.println("Called here");

                String search = newText.toString().trim();

*//*                if(search.length() > 0) {

                    adapter.getFilter().filter(newText);
                }*//*

                adapter.getFilter().filter(newText);

                return true;

            }
            @Override
            public boolean onQueryTextSubmit(String query)
            {


                String search = query.toString().trim();

                *//*if(search.length() > 0) {

                    adapter.getFilter().filter(query);
                }*//*

                adapter.getFilter().filter(search);

                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);*/
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_filter:

                View menuItemView =getActivity().findViewById(R.id.action_filter); // SAME ID AS MENU ID
                PopupMenu   popupMenu = new PopupMenu(getActivity(), menuItemView);
                for (Map.Entry<String, String> entry : hashMapLbl.entrySet()) {
                    popupMenu.getMenu().add(entry.getValue());
                }
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        System.out.println("itemClicked   "+item);
                        for (Map.Entry<String, String> entry : hashMapLbl.entrySet()) {
                            if(entry.getValue().equals(item.getTitle()))
                            {
                                getVisitorData(getActivity(),entry.getKey());
                                mListener.onFragmentInteraction(entry.getValue(),type);
                            }
                        }
                        return true;
                    }
                });
                break;

        }
        return super.onOptionsItemSelected(item);
    }




}
