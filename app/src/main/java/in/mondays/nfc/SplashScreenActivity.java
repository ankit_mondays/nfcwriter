package in.mondays.nfc;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

import in.mondays.nfc.Util.Global;


public class SplashScreenActivity extends Activity {

    private ProgressBar progressBar;
    private SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        initGUI();

                 new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {




                    if (sp.getBoolean(Global.IS_LOGGED_IN, false)) // this means user has already verified the mobile number
                    {
                        Intent in = new Intent(getApplicationContext(), MainActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(in);
                     //   overridePendingTransition(0, 0);



                    } else
                    {
                        Intent in = new Intent(getApplicationContext(), VerificationActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(in);
                    }

                }
            }, 3000);

          }

    public void initGUI() {
        progressBar = (ProgressBar) findViewById(R.id.pb_splashScreen);
        sp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);

        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#009A9A"), android.graphics.PorterDuff.Mode.MULTIPLY);






    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}
