package in.mondays.nfc;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import in.mondays.nfc.CustomControls.SlidingTabLayout;
import in.mondays.nfc.Fragments.AccessFragment;
import in.mondays.nfc.Fragments.StudentFragment;
import in.mondays.nfc.Util.CommonMethods;
import in.mondays.nfc.Util.Global;
import in.mondays.nfc.adapter.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity implements AccessFragment.OnFragmentInteractionListener,StudentFragment.OnFragmentInteractionListener {




    PopupMenu popupMenu;
    DatabaseReference lblRef,moduleRef;
    SharedPreferences loginsp;
    ValueEventListener deviceVerRefListner,deviceDeregisterListner,moduleRefListner;
    DatabaseReference deviceDeregister;
    DatabaseReference deviceVerRef;
    DatabaseReference visitorRef=null;
    ChildEventListener vizChildListner=null;
    ViewPager pager;
    TextView tv_accessTyp;
    ArrayList<String> groupListName;

    SlidingTabLayout tabs;
    int Numboftabs = 2;
    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        intGUI();
        isStoragePermissionGranted();
        getDatafromFirebase(MainActivity.this);
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.NFC)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.INTERNET)
                    == PackageManager.PERMISSION_GRANTED

                    ) {

                return true;
            } else {


                ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE",
                        "android.permission.READ_EXTERNAL_STORAGE", "android.permission.NFC","android.permission.INTERNET"}, 1);
                return false;
            }
        } else {

            return true;
        }

    }

    private void intGUI()
    {

        tv_accessTyp=(TextView)findViewById(R.id.tv_accessTyp);
        loginsp=getSharedPreferences(Global.LOGIN_SP,Context.MODE_PRIVATE);
        lblRef= PersistentDatabase.getDatabase().getReference("label/"+
                loginsp.getString(Global.BIZ_ID,"")+"/"+loginsp.getString(Global.GROUP_ID,"")+"/access");
        lblRef.keepSynced(true);
        tv_accessTyp.setText("ACCESS");
        Bundle b=new Bundle();
        b.putString("type","ACCESS");
        loginsp.edit().putInt(Global.SELECTED_TYPE,0).apply();
        Fragment fragment =  new AccessFragment();
        fragment.setArguments(b);
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
        groupListName=new ArrayList<>();
/*

        pager = (ViewPager)findViewById(R.id.pager);

        CharSequence Titles[] = {"ACCESS", "PARENT"};

        if(viewPagerAdapter==null) {
            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);


        }

        pager.setAdapter(viewPagerAdapter);
        pager.setOffscreenPageLimit(2);

        tabs = (SlidingTabLayout)findViewById(R.id.tabs);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.app_tab_strip_color);
            }
        });

        tabs.setDistributeEvenly(true);
        tabs.setViewPager(pager);
*/




        //  editSearch = (EditText)findViewById(R.id.edittext_search);



      /*  editSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (adapter != null) {
                    adapter.ic_filter(s.toString().trim(),accessList); // filtering the list items

                    // condition for changing the drawable inside editext
                    if (s.toString().trim().length() == 0) {
                        editSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.search_icon, 0);
                    } else {

                        editSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cross_icon, 0);
                    }
                }
            }
        });

        editSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= ((editSearch.getRight() + 2) - editSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        editSearch.setText("");

                        return true;
                    }
                }
                return false;
            }
        });*/


    }

    public void getDatafromFirebase(final Context context) {


        moduleRef=PersistentDatabase.getDatabase().getReference("biz/"+loginsp.getString(Global.BIZ_ID,"")+"/mod/"
        );

        if(moduleRefListner!=null)
        {
            moduleRef.removeEventListener(moduleRefListner);

        }


        moduleRefListner=moduleRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                groupListName.clear();
                for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    if(dataSnapshot1.getKey().equals("access") || dataSnapshot1.getKey().equals("parent")
                            || dataSnapshot1.getKey().equals("student") || dataSnapshot1.getKey().equals("resident")) {
                        if(dataSnapshot1.getKey().equals("access"))
                            groupListName.add(0,dataSnapshot1.getKey().toUpperCase());
                        else
                            groupListName.add(dataSnapshot1.getKey().toUpperCase());
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        deviceVerRef = PersistentDatabase.getDatabase().
                getReference("device/" + loginsp.getString(Global.BIZ_ID, "")+"/"
                        +Global.APP_NAME
                        + "/" + loginsp.getString(Global.UUID, "") + "/l");

        if(deviceVerRefListner!=null)
            deviceVerRef.removeEventListener(deviceVerRefListner);


        deviceVerRefListner =   deviceVerRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                try {

                    if (loginsp.getBoolean(Global.IS_LOGGED_IN, false)
                            && dataSnapshot != null && dataSnapshot.getValue() != null
                            && !dataSnapshot.getValue().toString().equalsIgnoreCase("")) {

                        String version =   new CommonMethods().getVerisonNumber(context);
                        DatabaseReference verRef = PersistentDatabase.getDatabase()
                                .getReference("device/" + loginsp.getString(Global.BIZ_ID, "")
                                        +"/"+Global.APP_NAME
                                        + "/" + loginsp.getString(Global.UUID, ""));

                        HashMap<String, Object> hashMap = new HashMap<String, Object>();
                        if(dataSnapshot.getValue().toString().equalsIgnoreCase("ver"))
                            hashMap.put(dataSnapshot.getValue().toString(), version);
                        if(dataSnapshot.getValue().toString().equalsIgnoreCase("now"))
                            hashMap.put("dt", Calendar.getInstance().getTimeInMillis());
                        verRef.updateChildren(hashMap);


                        deviceVerRef.setValue("");

                    }

                } catch (Exception e) {

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        ////////////////////////////// logout from server /////////////////////////////////////////
        deviceDeregister = PersistentDatabase.getDatabase().getReference("device/" +
                loginsp.getString(Global.BIZ_ID, "")
                +"/"+Global.APP_NAME
                + "/" + loginsp.getString(Global.UUID, "")+
                "/status");

        deviceDeregisterListner = deviceDeregister.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {


                    if (loginsp.getBoolean(Global.IS_LOGGED_IN, false)
                            && dataSnapshot != null &&
                            !Boolean.parseBoolean(dataSnapshot.getValue().toString()))

                    {

                        new CommonMethods().logout(MainActivity.this);
                        Intent in = new Intent(MainActivity.this, VerificationActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        startActivity(in);
                    }
                } catch (Exception e) {

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
      /*

            //////////////////////////////getting group name////////////////////////////////


            String uuid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            loginSp.edit().putString(uuid, uuid).apply();
            DatabaseReference deviceRef = PersistentDatabase.getDatabase().getReference
                    ("device/" + loginSp.getString(Global.BIZ_ID + "_0", "")
                            +"/"+Global.APP_NAME
                            +"/"+loginSp.getString(Global.UUID, uuid)
                    );
            if(deviceNameListner!=null)
                deviceRef.removeEventListener(deviceNameListner);

            deviceNameListner= deviceRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    try {
                        tv_deviceName.setText(dataSnapshot.child("nm").
                                getValue().toString());


                    } catch (Exception e) {
                        tv_deviceName.setText(loginSp.getString(Global.DEVICE_NAME, ""));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), Global.SERVER_ERROR_MSG, Toast.LENGTH_SHORT).show();
        }


        ///////////////////////// getting biz data /////////////////////////////////////

       /* DatabaseReference bizImgRef = PersistentDatabase.getDatabase().
                getReference("biz/" + loginSp.getString(Global.BIZ_ID + "_0", "")
                );

        if(bizImageListner!=null)
            bizImgRef.removeEventListener(bizImageListner);

        bizImageListner=  bizImgRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    if (orgimageLoader == null)
                        orgimageLoader = new ImageLoaderOriginal(getApplicationContext(), "logo");   // this class use to take the image from url
                    if(dataSnapshot.hasChild("img")) {
                        loginsp.edit().putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue().toString().trim()).commit();
                        orgimageLoader.DisplayImage(loginsp.getString(Global.BIZ_COMPANY_IMG, ""), UserImage);

                    }
                    profileName.setText(dataSnapshot.child("nm").getValue().toString());

                    tv_groupName.setText(dataSnapshot.child("grp")
                            .child(loginSp.getString(Global.GROUP_ID,"")).child("nm").getValue().toString());


                } catch (Exception e) {
                    if (orgimageLoader == null)

                        orgimageLoader = new ImageLoaderOriginal(getApplicationContext());   // this class use to take the image from url

                    orgimageLoader.DisplayImage(loginsp.getString(Global.BIZ_COMPANY_IMG, ""), UserImage);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(deviceDeregisterListner!=null)
            deviceDeregister.removeEventListener(deviceDeregisterListner);

        if(deviceVerRefListner!=null)
            deviceVerRef.removeEventListener(deviceVerRefListner);
        if(vizChildListner!=null)
        {
            visitorRef.removeEventListener(vizChildListner);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_accessType:

                android.app.AlertDialog.Builder b = new android.app.AlertDialog.Builder(this);
                b.setTitle("Select Access Type");

                b.setSingleChoiceItems(groupListName.toArray(new String[groupListName.size()]),
                        loginsp.getInt(Global.SELECTED_TYPE,0), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                loginsp.edit().putInt(Global.SELECTED_TYPE,i).apply();
                                dialogInterface.dismiss();
                                if(!groupListName.get(i).equals("STUDENT"))
                                {
                                    Fragment fragment=new AccessFragment();
                                    Bundle bundle= new Bundle();
                                    bundle.putString("type",groupListName.get(i));
                                    fragment.setArguments(bundle);
                                    tv_accessTyp.setText(groupListName.get(i));
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
                                }
                                if(groupListName.get(i).equals("STUDENT"))
                                {
                                    Fragment fragment=new StudentFragment();
                                    Bundle bundle= new Bundle();
                                    bundle.putString("type",groupListName.get(i));
                                    fragment.setArguments(bundle);
                                    tv_accessTyp.setText(groupListName.get(i));
                                    tv_accessTyp.setText("STUDENT");
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
                                }

                            }
                        });
                b.show();
                return true;




            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onFragmentInteraction(String lbl,String type) {
        if(lbl.equals("All"))
            tv_accessTyp.setText(type.toUpperCase());
        else
        tv_accessTyp.setText(type.toUpperCase()+"("+lbl+")");
    }
}
