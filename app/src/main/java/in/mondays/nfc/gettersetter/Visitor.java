package in.mondays.nfc.gettersetter;

import java.util.HashMap;

public class Visitor implements Comparable<Visitor> {

    String act,name, toMeet, mobile, purpose, barCode, inTime, outTime, isOutDateUpdated, photoUrl, purposeId, photoPath,visitorId;
    int isVerified;
    boolean issued;
    String status_check, Status_UpdateCheck;

    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }

    public String getStatus_UpdateCheck() {
        return Status_UpdateCheck;
    }

    public void setStatus_UpdateCheck(String status_UpdateCheck) {
        Status_UpdateCheck = status_UpdateCheck;
    }

    public String getStatus_check() {
        return status_check;
    }

    public void setStatus_check(String status_check) {
        this.status_check = status_check;
    }

    byte[] photo;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    private String rowId;

    public String getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    HashMap<String, String> columnDataHashMap;
    String[] columnId, columnName, columnData, columnDataType, columnUiGroup;
    int[] fieldMandatory;

    public String getResidentId() {
        return residentId;
    }

    public void setResidentId(String residentId) {
        this.residentId = residentId;
    }

    String residentId;

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String[] getColumnDataType() {
        return columnDataType;
    }

    public void setColumnDataType(String[] columnDataType) {
        this.columnDataType = columnDataType;
    }


    public String[] getColumnUiGroup() {
        return columnUiGroup;
    }

    public void setColumnUiGroup(String[] columnUiGroup) {
        this.columnUiGroup = columnUiGroup;
    }

    public int[] getFieldMandatory() {
        return fieldMandatory;
    }

    public void setFieldMandatory(int[] fieldMandatory) {
        this.fieldMandatory = fieldMandatory;
    }

    public void setColumnData(String[] columnData) {
        this.columnData = columnData;
    }

    public String[] getColumnData() {
        return columnData;
    }


    public HashMap<String, String> getColumnDataHashMap() {
        return columnDataHashMap;
    }

    public void setColumnDataHashMap(HashMap<String, String> columnDataHashMap) {
        this.columnDataHashMap = columnDataHashMap;
    }

    public void setColumnId(String[] columnId) {
        this.columnId = columnId;
    }

    public String[] getColumnId() {
        return columnId;
    }

    public void setColumnName(String[] columnName) {
        this.columnName = columnName;
    }

    public String[] getColumnName() {
        return columnName;
    }

    public void setIsVerified(int isVerified) {
        this.isVerified = isVerified;
    }

    public int getIsVerified() {
        return isVerified;
    }

    public void setIsOutDateUpdated(String isOutDateUpdated) {
        this.isOutDateUpdated = isOutDateUpdated;
    }

    public String getIsOutDateUpdated() {
        return isOutDateUpdated;
    }


    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }


    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getInTime() {
        return inTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToMeet() {
        return toMeet;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setPurposeId(String purposeId) {
        this.purposeId = purposeId;
    }

    public String getPurposeId() {
        return purposeId;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public void setToMeet(String toMeet) {
        this.toMeet = toMeet;
    }


    public void setIssued(boolean issued) {
        this.issued = issued;
    }

    public boolean issued() {
        return issued;
    }

    @Override
    public int compareTo(Visitor visitor) {
        return getInTime().compareTo(visitor.getInTime());
    }
}
