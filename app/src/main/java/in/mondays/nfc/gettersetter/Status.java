package in.mondays.nfc.gettersetter;

/**id
residentId
name
mobile
photo
status(allow, always allow, block)*/

public class Status {

	private String residentId;

	private String visitorName;

	private String visitorMobile;

    public byte[] getVisitorPhoto() {
        return visitorPhoto;
    }

    public void setVisitorPhoto(byte[] visitorPhoto) {
        this.visitorPhoto = visitorPhoto;
    }

    private byte[] visitorPhoto;

	private String status;
	private String note;


	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	public String getResidentId() {
		return residentId;
	}

	public void setResidentId(String residentId) {
		this.residentId = residentId;
	}

	public String getVisitorName() {
		return visitorName;
	}

	public void setVisitorName(String visitorName) {
		this.visitorName = visitorName;
	}

	public String getVisitorMobile() {
		return visitorMobile;
	}

	public void setVisitorMobile(String visitorMobile) {
		this.visitorMobile = visitorMobile;
	}



	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


}
