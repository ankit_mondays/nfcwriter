package in.mondays.nfc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import in.mondays.nfclibrary.NFCAsyncTaskCompleteListner;
import in.mondays.nfclibrary.NFCReaderWriter;
import in.mondays.nfc.Util.CommonMethods;
import in.mondays.nfc.Util.Global;

public class NFCwriterActivity extends AppCompatActivity implements NFCAsyncTaskCompleteListner{

    NFCReaderWriter nfcReaderWriter;
    String tag=null,nm="",type;
    TextView tag_written;
    DatabaseReference accessRef,parentRef,studentRef,residentRef;
    SharedPreferences loginSp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfcwriter);
        tag_written=(TextView)findViewById(R.id.tag_written);
        nfcReaderWriter= new NFCReaderWriter(NFCwriterActivity.this);
        loginSp=getSharedPreferences(Global.LOGIN_SP,MODE_PRIVATE);
        accessRef=PersistentDatabase.getDatabase().getReference("access/"+
                loginSp.getString(Global.BIZ_ID,"")+"/"+loginSp.getString(Global.GROUP_ID,""));
        parentRef=PersistentDatabase.getDatabase().getReference("parent/"+
                loginSp.getString(Global.BIZ_ID,"")+"/"+loginSp.getString(Global.GROUP_ID,""));
        studentRef=PersistentDatabase.getDatabase().getReference("student/"+
                loginSp.getString(Global.BIZ_ID,"")+"/"+loginSp.getString(Global.GROUP_ID,""));

       residentRef=PersistentDatabase.getDatabase().getReference("resident/"+
                loginSp.getString(Global.BIZ_ID,"")+"/"+loginSp.getString(Global.GROUP_ID,""));
        if(getIntent()!=null && getIntent().hasExtra("tag"))
        {
            tag=   getIntent().getStringExtra("tag");
        }
        if(getIntent()!=null && getIntent().hasExtra("nm"))
        {
            nm=   getIntent().getStringExtra("nm");
        }
        if(getIntent()!=null && getIntent().hasExtra("type"))
        {
            type=getIntent().getStringExtra("type");
        }

        getSupportActionBar().setTitle(nm);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(tag!=null &&  nfcReaderWriter.isNFCAvailable())
            nfcReaderWriter.writeNFC(intent,tag,"in.mondays",this);
        //  nfcReaderWriter.readNFC(intent,this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(nfcReaderWriter.isNFCAvailable())
            nfcReaderWriter.setupForegroundDispatch(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(nfcReaderWriter.isNFCAvailable())
            nfcReaderWriter.stopForegroundDispatch(this,nfcReaderWriter.getNfcAdapter());
    }

    @Override
    public void readNFCData(String tag) {
        tag_written.setText("Tag written: "+tag);

        CommonMethods.showToast(NFCwriterActivity.this,"Tag successfully written", Toast.LENGTH_SHORT);

        final String[] tags= tag.split("\\|");
        if(tags[0].equalsIgnoreCase("#"+Global.STAFF)) {

            Query query = accessRef.orderByChild("mob").equalTo(tags[1]);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                        accessRef.child(dataSnapshot1.getKey()).child("nfc").setValue(true);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

        if(tags[0].equalsIgnoreCase("#"+Global.PARENT))
        {

            Query query = parentRef.orderByChild("mob").equalTo(tags[1]);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                        parentRef.child(dataSnapshot1.getKey()).child("nfc").setValue(true);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        if(tags[0].equalsIgnoreCase("#"+Global.STUDENT))
        {
            Query query = studentRef.orderByKey().equalTo(tags[1]);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                        studentRef.child(dataSnapshot1.getKey()).child("nfc").setValue(true);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        if(tags[0].equalsIgnoreCase("#"+Global.RESIDENT))
        {

            Query query = residentRef.orderByChild("mob").equalTo(tags[1]);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                        residentRef.child(dataSnapshot1.getKey()).child("nfc").setValue(true);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }
}
