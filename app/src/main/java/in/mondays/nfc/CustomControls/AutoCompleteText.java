package in.mondays.nfc.CustomControls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * Created by developer on 12/8/16.
 */
public class AutoCompleteText extends AutoCompleteTextView{
    public AutoCompleteText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AutoCompleteText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AutoCompleteText(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Roboto-Light.ttf");
        setTypeface(tf);
    }
}
