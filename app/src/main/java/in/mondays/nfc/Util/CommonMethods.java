package in.mondays.nfc.Util;



/* this class will handle all the common methods used by other class*/

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import in.mondays.nfc.PersistentDatabase;


public class CommonMethods {


    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    private static int[] p0 = {0, 0x80};
    private static int[] p1 = {0, 0x40};
    private static int[] p2 = {0, 0x20};
    private static int[] p3 = {0, 0x10};
    private static int[] p4 = {0, 0x08};
    private static int[] p5 = {0, 0x04};
    private static int[] p6 = {0, 0x02};
    static String barcode = "";


    // this method is using zxing library to get the barcode pattern bitmap image






    // hide the keyboard

    public static void setHideSoftKeyboard(Activity activity, EditText ed) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ed.getWindowToken(), 0);
    }


    public static String getStringFromInputStream(InputStream stream) throws IOException {
        int n = 0;
        char[] buffer = new char[1024 * 4];
        InputStreamReader reader = new InputStreamReader(stream, "UTF8");
        StringWriter writer = new StringWriter();
        while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
        return writer.toString();
    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap, String sourcePath,String imageKey,Context context) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if (sourcePath != null) {
            try {
                bitmap = rotateBitmap(sourcePath, bitmap,imageKey,context);
            }catch (Exception e)
            {
                SharedPreferences imgPathSharedPref=
                        context.getSharedPreferences(Global.VIZ_IMAGE_SP,Context.MODE_PRIVATE);
                if(imageKey!=null && imgPathSharedPref.contains(imageKey)) {
                    try {
                        File WhoCameDirectory = new File(imgPathSharedPref.getString(imageKey, ""));

                        if (WhoCameDirectory.exists())
                            WhoCameDirectory.delete();

                    }
                    catch (Exception ex)
                    {
                        CommonMethods.writeToLogFile("13 "+ex.toString());
                    }
                    finally {
                        imgPathSharedPref.edit().remove(imageKey).apply();
                    }
                }


                CommonMethods.writeToLogFile("12 "+e.toString());
            }
        }
        bitmap.compress(CompressFormat.JPEG, 80, outputStream);

        return outputStream.toByteArray();
    }


    public static Bitmap rotateBitmap(String sourcepath, Bitmap bitmap,String imageKey,Context context) {
        int rotate = 0;

        try {
            File imageFile = new File(sourcepath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            //  e.printStackTrace();

            SharedPreferences imgPathSharedPref=
                    context.getSharedPreferences(Global.VIZ_IMAGE_SP,Context.MODE_PRIVATE);
            if(imageKey!=null && imgPathSharedPref.contains(imageKey)) {
                try {
                    File WhoCameDirectory = new File(imgPathSharedPref.getString(imageKey, ""));

                    if (WhoCameDirectory.exists())
                        WhoCameDirectory.delete();
                }catch (Exception ex)
                {
                    CommonMethods.writeToLogFile(ex.toString());
                }
                finally {
                    imgPathSharedPref.edit().remove(imageKey).apply();
                }

            }
            CommonMethods.writeToLogFile(e.toString());

        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);



        return bitmap;

    }

    public static String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }


   /* public static String getStringFromBitmap(Bitmap bitmapPicture) {
        *//*
         * This functions converts Bitmap picture to a string which can be
		 * JSONified.
		 * *//*
        final int COMPRESSION_QUALITY = 100;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }*/

    public static boolean isInternetWorking(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static byte[] eachLinePixToCmd(byte[] src, int nWidth, int nMode) {
        int nHeight = src.length / nWidth;
        int nBytesPerLine = nWidth / 8;
        byte[] data = new byte[nHeight * (8 + nBytesPerLine)];
        int offset = 0;
        int k = 0;
        for (int i = 0; i < nHeight; i++) {
            offset = i * (8 + nBytesPerLine);
            data[offset + 0] = 0x1d;
            data[offset + 1] = 0x76;
            data[offset + 2] = 0x30;
            data[offset + 3] = (byte) (nMode & 0x01);
            data[offset + 4] = (byte) (nBytesPerLine % 0x100);
            data[offset + 5] = (byte) (nBytesPerLine / 0x100);
            data[offset + 6] = 0x01;
            data[offset + 7] = 0x00;
            for (int j = 0; j < nBytesPerLine; j++) {
                data[offset + 8 + j] = (byte) (p0[src[k]] + p1[src[k + 1]]
                        + p2[src[k + 2]] + p3[src[k + 3]] + p4[src[k + 4]]
                        + p5[src[k + 5]] + p6[src[k + 6]] + src[k + 7]);
                k = k + 8;
            }
        }

        return data;
    }









    public String[] getSlipMessages(String type, Context context) {
        int size = 0;
        SharedPreferences loginsp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        String[] message = null;
        if (type.equalsIgnoreCase("footer")) {
            message = getFooterMessage(loginsp);
            size = message.length;
        }
        if (type.equalsIgnoreCase("header")) {
            message = getHeaderMessage(loginsp);
            size = message.length;
        }

        if (type.equalsIgnoreCase("branding")) {
            message = getBrandingMessage(loginsp);
            size = message.length;
        }


        String[] printMessage = new String[size];

        for (int i = 0; i < size; i++) {


            if (message[i].length() > 48) {
                char ch = message[i].charAt(48);

                if (ch != ' ') {

                    String msg1 = message[i].substring(0, 48);
                    if (message[i].contains(" ")) {
                        String msg3 = message[i].substring(0, msg1.lastIndexOf(" ") + 1);
                        String msg2 = message[i].substring(msg3.length(), message[i].length());
                        printMessage[i] = center(msg3.trim(), 48, " ") + "\n" + center(msg2, 48, " ") + "\n";

                    } else {
                        String msg2 = message[i].substring(msg1.length() + 1, message[i].length());
                        printMessage[i] = center(msg1.trim(), 48, " ") + "\n" + center(msg2.trim(), 48, " ") + "\n";
                    }

                }
            } else {
                printMessage[i] = center(message[i].trim(), 48, " ");

            }
        }

        return printMessage;
    }


    public String center(String s, int size, String pad) {

        if (pad == null)
            throw new NullPointerException("pad cannot be null");
        if (pad.length() <= 0)
            throw new IllegalArgumentException("pad cannot be empty");
        if (s == null || size <= s.length())
            return s;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < (size - s.length()) / 2; i++) {
            sb.append(pad);
        }
        sb.append(s);
        while (sb.length() < size) {
            sb.append(pad);

        }

        return sb.toString();
    }


    public String[] getFooterMessage(SharedPreferences loginsp) {


        int size = loginsp.getInt(Global.SETTINGS_SLIP_FOOTER_MSG_SIZE, 0);
        String array[] = null;

        if (size > 0) {
            array = new String[size];
            for (int i = 0; i < size; i++)
                array[i] = loginsp.getString(Global.SETTINGS_SLIP_FOOTER_LINE + i, null);
        } else {
            array = new String[2];
            array[0] = "Please return slip while leaving";
        }
        return array;

    }


    public String[] getHeaderMessage(SharedPreferences loginsp) {


        int size = loginsp.getInt(Global.SETTINGS_SLIP_HEADER_MSG_SIZE, 0);
        String array[] = null;

        if (size > 0) {
            array = new String[size];
            for (int i = 0; i < size; i++)
                array[i] = loginsp.getString(Global.SETTINGS_SLIP_HEADER_LINE + i, null);
        } else {
            array = new String[2];
            array[0] = loginsp.getString(Global.BIZ_COMPANY_NAME, "");
        }
        return array;

    }


    public String[] getBrandingMessage(SharedPreferences loginsp) {


        int size = loginsp.getInt(Global.SETTINGS_SLIP_BRANDING_MSG_SIZE, 0);
        String array[] = null;

        if (size > 0) {
            array = new String[size];
            for (int i = 0; i < size; i++)
                array[i] = loginsp.getString(Global.SETTINGS_SLIP_BRANDING_LINE + i, null);
        } else {
            array = new String[2];
            array[0] = "Tech Partners WhoCame.In & Mondays.In";
        }
        return array;

    }







    //update out time of visitor





    public String getTimeDiff(long mills, Date currentDt, Date frDate, Date toDate) {
        String isBeforeOrEarly = "";
        if (currentDt.before(frDate)) {
            mills = frDate.getTime() - currentDt.getTime();
            isBeforeOrEarly = "Early by";
        }
        if (currentDt.after(toDate)) {
            mills = currentDt.getTime() - frDate.getTime();
            isBeforeOrEarly = "Late by";
        }
        int Hours = (int) (mills / (1000 * 60 * 60));
        int Mins = (int) (mills / (1000 * 60)) % 60;
        String time;

        if (Hours == 0) {
            time = isBeforeOrEarly + " " + Mins + " mins";
        } else if (Mins == 0) {
            time = isBeforeOrEarly + " " + Hours + " hrs";
        } else {
            time = isBeforeOrEarly + " " + Hours + ":" + Mins + " hrs";
        }

        return time;
    }


    public static boolean isTimeExceeded( long currentDt, long inDt, long timeLimit ) {

        Date cDt= new Date(currentDt);
        Date iDt= new Date(inDt);


        long  mills = cDt.getTime() - iDt.getTime();

        if (mills > (timeLimit)) {
            return true;
        } else {
            return false;
        }




    }





    public static Calendar getTodaysDate() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.MILLISECOND, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.HOUR_OF_DAY, 0);

        return today;
    }



    public String getVerisonNumber(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionName;
    }

    public void logout(Context context) {

        SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        String uuid = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        DatabaseReference codeRef = PersistentDatabase.getDatabase().getReference("device/"+
                loginSp.getString(Global.BIZ_ID, "")+"/"+Global.APP_NAME+ "/" + uuid);


        codeRef.getRef().child("del").setValue(true);
        codeRef.getRef().child("dt").setValue(Calendar.getInstance().getTimeInMillis());
        SharedPreferences sp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorlogin = sp.edit();
        editorlogin.clear();
        editorlogin.commit();

        //FirebaseAuth.getInstance().signOut();
        File listFile = new File(
                Environment.getExternalStorageDirectory()+File.separator+
                        "NFC_Writer"+File.separator+"accesslist");
        if (listFile.exists())
            DeleteRecursive(listFile);



        deleteCache(context);
    }


    public static void DeleteRecursive(File fileOrDirectory)
    {
        if (fileOrDirectory.isDirectory())
        {
            for (File child : fileOrDirectory.listFiles())
            {
                DeleteRecursive(child);
            }
        }

        fileOrDirectory.delete();
    }

/*    public void deleteCache(Context context) {
        File cache = context.getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));

                }
            }
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }*/


    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }








    public String getDate(long milliSeconds, String dateFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    // check for sim card
    private boolean checkForSimCard(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int SIM_STATE = telephonyManager.getSimState();
        if (SIM_STATE == TelephonyManager.SIM_STATE_READY)
            return true;
        else {
            switch (SIM_STATE) {
                case TelephonyManager.SIM_STATE_ABSENT: //SimState = "No Sim Found!";
                    break;
                case TelephonyManager.SIM_STATE_NETWORK_LOCKED: //SimState = "Network Locked!";
                    break;
                case TelephonyManager.SIM_STATE_PIN_REQUIRED: //SimState = "PIN Required to access SIM!";
                    break;
                case TelephonyManager.SIM_STATE_PUK_REQUIRED: //SimState = "PUK Required to access SIM!"; // Personal Unblocking Code
                    break;
                case TelephonyManager.SIM_STATE_UNKNOWN: //SimState = "Unknown SIM State!";
                    break;
            }
            return false;
        }
    }






    public static  Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static void writeToLogFile(String log)
    {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Who_Came");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "log");
            FileWriter writer = new FileWriter(gpxfile,true);
            writer.append("dt: "+Calendar.getInstance().getTime()+"\n"+log+"\n");
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void showToast(Context context, String msg,int time)
    {
        Toast toast= Toast.makeText(context,msg,time);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }




    public static void uploadImgTask(final Context context, final String key, StorageReference storageref, String imgPath,
                                     final DatabaseReference profileRef, final DatabaseReference visitorRef,
                                     final DatabaseReference mobBizRef, final String fieldKey)
    {

        final SharedPreferences imgPathSharedPref= context.getSharedPreferences(Global.VIZ_IMAGE_SP,Context.MODE_PRIVATE);
        if(!imgPathSharedPref.contains(key))
        imgPathSharedPref.edit().putString(key,imgPath).apply();

        try {

            UploadTask uploadTask = storageref.putBytes(CommonMethods.getBitmapAsByteArray
                    (decodeSampledBitmapFromFile(imgPath, 300, 400),
                            imgPath, key, context));
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    HashMap<String, Object> updatehashmap = new HashMap<String, Object>();
                    updatehashmap.put(fieldKey, taskSnapshot.getDownloadUrl().toString());
                 /*   HashMap<String, Object> profMap = new HashMap<String, Object>();
                    profMap.put(fieldKey, taskSnapshot.getDownloadUrl().toString());*/

                    if(profileRef!=null)
                        profileRef.updateChildren(updatehashmap);
                    if(mobBizRef!=null)
                        mobBizRef.updateChildren(updatehashmap);
                    visitorRef.updateChildren(updatehashmap);

                    try {
                        File WhoCameDirectory = new File(imgPathSharedPref.getString(key,""));
                        if (WhoCameDirectory.getAbsoluteFile().exists()) {
                            WhoCameDirectory.getAbsoluteFile().delete();
                        }

                    }catch (Exception e)
                    {
                        CommonMethods.writeToLogFile("9 "+e.toString()); // writing to log file
                    }
                    finally {
                        imgPathSharedPref.edit().remove(key).apply();

                    }

                }
            });

        }catch (Exception e)
        {
            if(imgPathSharedPref.contains(key)) {
                try {
                    File WhoCameDirectory = new File(imgPathSharedPref.getString(key, ""));
                    if (WhoCameDirectory.exists())
                        WhoCameDirectory.delete();
                }
                catch (Exception ex)
                {
                    CommonMethods.writeToLogFile("10 "+ex.toString());
                }
                finally {
                    imgPathSharedPref.edit().remove(key).apply();

                }
            }


            CommonMethods.writeToLogFile("11 "+e.toString()); // writing to log file
        }
    }

    public static String getAccessType(String accessType)
    {
switch (accessType)
{
    case "PARENT":
        return Global.PARENT;
    case "RESIDENT":
        return Global.RESIDENT;
    case "STUDENT":
        return Global.STUDENT;
    case "ACCESS":
        return Global.STAFF;
    default:
        return Global.STAFF;
}
    }


}


