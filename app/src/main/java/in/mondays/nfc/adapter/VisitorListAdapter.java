package in.mondays.nfc.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import in.mondays.nfc.ImageLoader.ImageLoaderOriginal;
import in.mondays.nfc.NFCwriterActivity;
import in.mondays.nfc.R;
import in.mondays.nfc.Util.Global;
import in.mondays.nfc.gettersetter.Visitor;


/**
 * Created by developer on 13/9/16.
 */
public class VisitorListAdapter extends RecyclerView.Adapter<VisitorListAdapter.MyViewHolder>
        implements Filterable {

    ArrayList<Visitor> visitorsList, tempVisitorList;
    Context context;
    private ArrayList<Visitor> filterData;
    SharedPreferences loginSp, imgPathSharedPref;
    ImageLoaderOriginal orgimageLoader;
    Bitmap bitmap;
    Bitmap picBitmap;
    String type;

    public VisitorListAdapter(Context context, ArrayList<Visitor> visitorsList, String type) {
        this.visitorsList = visitorsList;
        //tempVisitorList=visitorsList;
        this.context = context;
        this.type = type;
        loginSp = getContext().getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        tempVisitorList = new ArrayList<>();
        filterData = new ArrayList<>();

        imgPathSharedPref = getContext().
                getSharedPreferences(Global.VIZ_IMAGE_SP, Context.MODE_PRIVATE);
        orgimageLoader = new ImageLoaderOriginal(getContext());

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.custom_invisitor_list, parent, false);

        // Return a new holder instance
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, final int position) {

        viewHolder.number.setText(visitorsList.get(position).getMobile());
        viewHolder.name.setText(visitorsList.get(position).getName());

        if (visitorsList.get(position).getPhotoUrl() != null) {// this class use to take the image from url
            orgimageLoader.DisplayImage(visitorsList.get(position).getPhotoUrl(), viewHolder.visitorPic);
        } else {
            viewHolder.visitorPic.setImageResource(0);
            viewHolder.visitorPic.setImageResource(R.drawable.default_user_picture);
        }

        viewHolder.llvisitorList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NFCwriterActivity.class);
                intent.putExtra("tag", "#" + type + "|" + visitorsList.get(position).getMobile());
                intent.putExtra("type", type);
                intent.putExtra("nm", visitorsList.get(position).getName());
                context.startActivity(intent);
            }
        });


        if (visitorsList.get(position).issued()) {
            viewHolder.iv_tick.setVisibility(View.VISIBLE);
        } else {
            viewHolder.iv_tick.setVisibility(View.GONE);
        }


    }

    private Context getContext() {
        return context;
    }

    @Override
    public int getItemCount() {
        return visitorsList.size();
    }

    public void customDatasetChanged(){
        this.notifyDataSetChanged();
        System.out.println("VisitorsCusList  "+visitorsList.size()+"  TempVisitors  "+tempVisitorList.size()+"  FilterData  "+filterData.size());
        tempVisitorList.clear();
        tempVisitorList.addAll(visitorsList);
    }

    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();

                filterData.clear();

                System.out.println("VisitorsList  "+visitorsList.size()+"  TempVisitors  "+tempVisitorList.size()+"  FilterData  "+filterData.size());

                if(constraint.length()==0){
                    filterData.addAll(tempVisitorList);
                }else {
                            for (final Visitor visitor : tempVisitorList) {
                                if (visitor.getMobile().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                                        visitor.getName().toLowerCase().contains(constraint.toString().toLowerCase()))
                                    filterData.add(visitor);
                            }
                }
                oReturn.count = filterData.size();
                oReturn.values = filterData;
                System.out.println("VisitorsList2 "+visitorsList.size()+"   TempVisitors2   "+tempVisitorList.size()+"   FilterData2   "+filterData.size());
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                visitorsList.clear();
                visitorsList.addAll((ArrayList<Visitor>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView number, name, purpose, toMeet, inTime;
        ImageView visitorPic, iv_tick;
        LinearLayout llvisitorList, getLlvisitorListItems;


        public MyViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_visitorListName);
            number = (TextView) itemView.findViewById(R.id.tv_visitorListMobile);
            //   purpose=(TextView)itemView.findViewById(R.id.tv_visitorListPurpose);
            visitorPic = (ImageView) itemView.findViewById(R.id.iv_visitorListPic);
            // barcode = (ImageView) itemView.findViewById(R.id.iv_visitorListBarcode);
            visitorPic = (ImageView) itemView.findViewById(R.id.iv_visitorListPic);
            llvisitorList = (LinearLayout) itemView.findViewById(R.id.ll_visitorList);
            getLlvisitorListItems = (LinearLayout) itemView.findViewById(R.id.ll_visitorListItems);
            iv_tick = (ImageView) itemView.findViewById(R.id.iv_issued);
            //   print = (ImageView) itemView.findViewById(R.id.iv_invisitor_list_print);
            //  iv_invisitor_list_cross=(ImageView)itemView.findViewById(R.id.iv_invisitor_list_cross);
            //   iv_allow=(ImageView)itemView.findViewById(R.id.iv_invisitor_list_allow);
            //   iv_dontAllow=(ImageView)itemView.findViewById(R.id.iv_invisitor_list_dontAllow);
            // toMeet=(TextView)itemView.findViewById(R.id.tv_visitorListToMeet);
            //inTime=(TextView)itemView.findViewById(R.id.tv_visitorListInTime);


        }
    }


}
