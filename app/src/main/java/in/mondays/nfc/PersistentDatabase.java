package in.mondays.nfc;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by developer on 8/7/16.
 */
public class PersistentDatabase {

    private static FirebaseDatabase mDatabase;

    public static FirebaseDatabase getDatabase() {
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
            mDatabase.setPersistenceEnabled(true);
        }
        return mDatabase;
    }

}
