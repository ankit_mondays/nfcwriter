package in.mondays.nfc.ImageLoader;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.google.firebase.crash.FirebaseCrash;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import in.mondays.nfc.R;


public class ImageLoaderOriginal {

    Context context;
    String barcode;
    MemoryCache memoryCache = new MemoryCache();
    FileCache fileCache;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService;
    Handler handler = new Handler();// handler to display images in UI thread

    SharedPreferences sp;
    int stub_id = R.drawable.default_user_picture;

    public ImageLoaderOriginal(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
        this.context = context;

    }



    public ImageLoaderOriginal(Context context, String type) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
        this.context = context;

        if(type.equalsIgnoreCase("logo"))
        {
            stub_id = R.drawable.large_logo;
        }
        else
        {
            stub_id = R.drawable.default_user_picture;
        }
    }


    public void DisplayImage(String url, ImageView imageView) {
        if (imageView == null) {
            return;
        }

        imageViews.put(imageView, url);

        Bitmap bitmap = memoryCache.get(url);

        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);

            // vc.insertDownloadedImage(url, CommonMethods.getBitmapAsByteArray(bitmap, null));

        } else {
            queuePhoto(url, imageView);
            imageView.setImageResource(stub_id);
        }
    }


    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    public Bitmap getBitmap(String url) {
        File f = fileCache.getFile(url);

        // from SD cache
        Bitmap b = decodeFile(f);

        if (b != null)
            return b;

        // from web
        try {
            Bitmap bitmap = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            CopyStream(is, os);
            os.close();
            conn.disconnect();
            bitmap = decodeFile(f);

            return bitmap;
        } catch (Throwable ex) {
            if(ex instanceof FileNotFoundException)
            {
            }

            if (ex instanceof OutOfMemoryError) {
                memoryCache.clear();
                return null;
            }
        }

        return null;
    }

    // decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1 = new FileInputStream(f);
            BitmapFactory
                    .decodeStream(new FlushedInputStream(stream1), null, o);
            stream1.close();

            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 120;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 3 < REQUIRED_SIZE
                        || height_tmp / 3 < REQUIRED_SIZE)
                    break;
                width_tmp /= 3;
                height_tmp /= 3;
                scale *= 3;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            FileInputStream stream2 = new FileInputStream(f);
            Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
        } catch (FileNotFoundException e) {

           // FirebaseCrash.report(e);


        } catch (IOException e) {
            Log.e("IO Exception: ", e.getMessage());
          //  FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    // Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            try {
                if (imageViewReused(photoToLoad))
                    return;
                Bitmap bmp = getBitmap(photoToLoad.url);


                memoryCache.put(photoToLoad.url, bmp);
                if (imageViewReused(photoToLoad))
                    return;
                BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad,photoToLoad.url);
                handler.post(bd);
            } catch (Throwable th) {

            }
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        String url;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p,String url) {
            bitmap = b;
            photoToLoad = p;
            this.url=url;

        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            if (bitmap != null) {

                photoToLoad.imageView.setImageBitmap(bitmap);


                //commented by alka
               /* sp = context.getSharedPreferences("Login", context.MODE_PRIVATE);

                Integer BisId = Integer.parseInt(sp.getString("BisId",""));
                Integer residentId = Integer.parseInt(sp.getString("ResidentId",""));

                if(BisId != 0){

                    vc.insertDownloadedImage(photoToLoad.url, CommonMethods.getBitmapAsByteArray(bitmap, null));

                }else if(residentId != 0){


                }*/

            } else
                photoToLoad.imageView.setImageResource(stub_id);
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break; // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    public void setNoPreviewStubId(int resId) {
        this.stub_id = resId;
    }

    public int getNoPreviewStubId() {
        return stub_id;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            FirebaseCrash.report(ex);
        }
    }

}